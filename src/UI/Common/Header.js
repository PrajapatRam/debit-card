'use strict';
import React from 'react';
import { StyleSheet, TouchableOpacity, View, Image, Text } from 'react-native';
import { Colors, Images, Fonts } from '../../res';

export default props => {
	const topView = (
		<View style={styles.topView}>
			{props.showBack ? (
				<TouchableOpacity
					onPress={() => {
						props.navigation.goBack();
					}}
					style={{}}
				>
					<Image source={Images.BackIcon} style={styles.backIcon}></Image>
				</TouchableOpacity>
			) : (
				<View />
			)}
			<Image source={Images.Logo} style={styles.logoStyle}></Image>
		</View>
	);

	return (
		<View style={styles.container}>
			{topView}
			<Text style={[styles.textStyle, { marginTop: props.BackIcon ? 0 : -15 }]}>{props.title}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		width: '100%'
	},
	topView: { flexDirection: 'row', padding: 20, justifyContent: 'space-between' },
	backIcon: {
		tintColor: Colors.white,
		height: 50,
		width: 50,
		marginTop: -13,
		marginLeft: -15
	},
	logoStyle: {},
	textStyle: {
		marginLeft: 20,
		color: Colors.white,
		...Fonts.AvenirBold()
	}
});

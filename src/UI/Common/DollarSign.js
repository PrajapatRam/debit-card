'use strict';
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Colors, Fonts, Strings } from '../../res';

export default props => {
	return (
		<View style={styles.viewStyle}>
			<Text style={styles.textStyle}>{Strings.Dollar}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	viewStyle: {
		backgroundColor: Colors.green,
		borderRadius: 4,
		alignItems: 'center',
		paddingHorizontal: 15,
		height: 25
	},
	textStyle: {
		color: Colors.white,
		...Fonts.AvenirBold(14),
		lineHeight: 25
	}
});

'use strict';
import React from 'react';
import { StyleSheet, View, Text, Dimensions, Image } from 'react-native';
import { Colors, Fonts, Strings, Images } from '../../res';
const { width, height } = Dimensions.get('window');

export default props => {
	const { cardholdername, cardnumber, validthru, cvv } = props.cardInfo;
	const cardn = props.hideCardNumber
		? `●●●●   ●●●●   ●●●●   ${cardnumber?.match(/.{1,4}/g)[3]}`
		: cardnumber?.match(/.{1,4}/g).join('   ');
	const cvvText = props.hideCardNumber ? '***' : cvv;
	return (
		<View style={styles.viewStyle}>
			<Image source={Images.AspireLogo} style={styles.logoStyle} />
			<Text style={styles.nameStyle}>{cardholdername}</Text>
			<Text style={styles.numberStyle}>{cardn}</Text>
			<View style={{ flexDirection: 'row' }}>
				<Text style={styles.cvvText}>
					{'Thru: '}
					<Text style={{ letterSpacing: 1.56 }}>{validthru}</Text>
				</Text>
				<Text style={[styles.cvvText, { marginLeft: 30 }]}>
					{'CVV: '}
					<Text style={{ letterSpacing: 2.88 }}>{cvvText}</Text>
				</Text>
			</View>
			<Image source={Images.VisaLogo} style={[styles.logoStyle, { marginTop: 10 }]} />
		</View>
	);
};

const styles = StyleSheet.create({
	viewStyle: {
		position: 'absolute',
		top: height / 3.85,
		margin: 20,
		width: width - 40,
		backgroundColor: Colors.green,
		borderRadius: 13,
		padding: 25
	},
	logoStyle: {
		alignSelf: 'flex-end'
	},
	nameStyle: {
		color: Colors.white,
		...Fonts.AvenirBold(22),
		letterSpacing: 0.53,
		marginTop: 15
	},
	numberStyle: {
		color: Colors.white,
		...Fonts.AvenirDemiBold(),
		letterSpacing: 3.46,
		marginTop: 20
	},
	cvvText: {
		color: Colors.white,
		...Fonts.AvenirDemiBold(13),
		letterSpacing: 0.31,
		marginTop: 20
	}
});

'use strict';
import React, { useState } from 'react';
import { StyleSheet, TouchableWithoutFeedback, View, Animated, Easing } from 'react-native';
import { Colors } from '../../res';

export default props => {
	const [xValue] = useState(new Animated.Value(props.active ? 14 : 0));

	const animate = active => {
		Animated.timing(xValue, {
			toValue: active ? 0 : 14,
			duration: 200,
			easing: Easing.linear,
			useNativeDriver: true
		}).start();
	};

	return (
		<TouchableWithoutFeedback
			onPress={() => {
				animate(props.active);
				props.onPress();
			}}
		>
			<View style={[styles.toggleView, { backgroundColor: props.active ? Colors.green : Colors.grey }]}>
				<Animated.View useNative style={[styles.toggleCircle, { transform: [{ translateX: xValue }] }]}></Animated.View>
			</View>
		</TouchableWithoutFeedback>
	);
};

const styles = StyleSheet.create({
	toggleView: {
		height: 20,
		width: 34,
		borderRadius: 45,
		padding: 2
	},
	toggleCircle: {
		height: 16,
		width: 16,
		backgroundColor: Colors.white,
		borderRadius: 45
	}
});

'use strict';
import React from 'react';
import {
	StyleSheet,
	View,
	Text,
	TouchableOpacity,
	SafeAreaView,
	StatusBar,
	ScrollView,
	Dimensions,
	Image,
	ActivityIndicator
} from 'react-native';
import { ToggleButton, Header, DollarSign, DebitCard } from './Common';
import { Colors, Fonts, Strings, Images } from '../res';
import { APIManager } from '../Networking';
import { connect } from 'react-redux';
import { setCardInfo, setLimit } from '../Redux/cardSlice';
const { width, height } = Dimensions.get('window');

class DebitCardScreen extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			hideCardNumber: false,
			loading: true,
			cardFreezed: false
		};
	}

	componentDidMount = () => {
		this.getCard();
	};

	getCard = () => {
		APIManager.getCard('1')
			.then(res => {
				this.props.setCardInfo(res.cards.cardinfo);
				this.setState({ loading: false });
			})
			.catch(err => console.log(err));
	};

	setLimit = limit => {
		this.setState({ loading: true });
		APIManager.updateLimit('1', limit)
			.then(res => {
				this.props.setLimit(res.cards.limit);
				this.setState({ loading: false });
			})
			.catch(err => console.log(err));
	};

	renderBalance = () => (
		<View>
			<Text style={styles.availableBalanceStyle}>{Strings.availableBalance}</Text>
			<View style={styles.balanceView}>
				<DollarSign />
				<Text style={styles.amountText}>{this.props.cardInfo.balance}</Text>
			</View>
		</View>
	);

	renderHideButton = () => {
		const image = this.state.hideCardNumber ? Images.EyeOpen : Images.EyeClosed;
		const text = this.state.hideCardNumber ? Strings.showCardNumber : Strings.hideCardNumber;
		return (
			<TouchableOpacity
				onPress={() => {
					this.setState({ hideCardNumber: !this.state.hideCardNumber });
				}}
				style={styles.hideButtonStyle}
			>
				<Image source={image}></Image>
				<Text style={styles.hideButtonText}>{text}</Text>
			</TouchableOpacity>
		);
	};

	renderCard() {
		return (
			<ScrollView style={{ position: 'absolute' }} contentContainerStyle={{}}>
				<View style={{ height: height / 4.5 }}></View>
				{this.renderHideButton()}
				<View
					style={{
						height: 2 * height,
						width,
						backgroundColor: Colors.white,
						borderTopLeftRadius: 24,
						borderTopRightRadius: 24
					}}
				>
					{this.renderCardOptions()}
				</View>
				<DebitCard hideCardNumber={this.state.hideCardNumber} cardInfo={this.props.cardInfo} />
			</ScrollView>
		);
	}

	renderRow = (image, title, subtitle, toggleButton) => {
		return (
			<View style={styles.cardOptionStyle}>
				<View style={{ flexDirection: 'row', width: width - 100 }}>
					<Image source={image}></Image>
					<View style={{ paddingHorizontal: 14 }}>
						<Text style={styles.title}>{title}</Text>
						<Text style={styles.subtitle}>{subtitle}</Text>
					</View>
				</View>
				{toggleButton || null}
			</View>
		);
	};

	renderLimitMeter = () => {
		if (!this.props.limit) return;
		const width = (this.props.cardInfo?.used?.replaceAll(',', '') / this.props.limit?.replaceAll(',', '')) * 100;
		return (
			this.props.limit && (
				<View>
					<View style={styles.spendMeter}>
						<Text style={{ color: Colors.black, ...Fonts.AvenirMedium(13) }}>{Strings.spend}</Text>
						<Text style={{ color: Colors.green, ...Fonts.AvenirDemiBold(13) }}>
							{'$' + this.props.cardInfo.used}
							<Text style={{ color: Colors.grey, ...Fonts.AvenirMedium(13) }}>{' | $' + this.props.limit}</Text>
						</Text>
					</View>
					<View style={styles.meter}>
						<View
							style={{
								width: `${width}%`,
								height: 20,
								borderRadius: 10,
								backgroundColor: Colors.green
							}}
						></View>
					</View>
				</View>
			)
		);
	};

	renderCardOptions = () => {
		const limitToggle = (
			<ToggleButton
				active={this.props.limit}
				onPress={() => {
					if (this.props.limit) {
						this.setLimit(null);
					} else {
						this.props.navigation.navigate('SetLimitScreen');
					}
				}}
			/>
		);
		const freezeToggle = (
			<ToggleButton
				active={this.state.cardFreezed}
				onPress={() => {
					this.setState({ cardFreezed: !this.state.cardFreezed });
				}}
			/>
		);
		const limitDesc = this.props.limit ? `${Strings.limitOnText + this.props.limit}` : Strings.limitOffText;
		const freezeDesc = this.state.cardFreezed ? Strings.freezeCardOn : Strings.freezeCardOff;
		return (
			<View style={{ marginTop: width / 2.5 }}>
				{this.renderLimitMeter()}
				{this.renderRow(Images.Insight, Strings.topUpAccount, Strings.topUpDesc)}
				{this.renderRow(Images.Limit, Strings.weeklySpend, limitDesc, limitToggle)}
				{this.renderRow(Images.Freeze, Strings.freezeCard, freezeDesc, freezeToggle)}
				{this.renderRow(Images.NewCard, Strings.getNewCard, Strings.getNewCardDesc)}
				{this.renderRow(Images.Deactivate, Strings.deactivatedCards, Strings.deactivatedCardsDesc)}
			</View>
		);
	};

	renderActivityIndicator = () =>
		this.state.loading && (
			<View style={styles.loading}>
				<ActivityIndicator size="large" />
			</View>
		);

	render() {
		return (
			<>
				<SafeAreaView style={styles.container}>
					<StatusBar barStyle="light-content" />
					<Header title={Strings.debitCard} />
					{this.renderBalance()}
					{this.renderCard()}

					{/* <ToggleButton active={this.state.active} onPress={() => this.setState({ active: !this.state.active })} /> */}
				</SafeAreaView>
				{this.renderActivityIndicator()}
			</>
		);
	}
}

const styles = StyleSheet.create({
	meter: {
		marginTop: 10,
		marginHorizontal: 20,
		height: 20,
		borderRadius: 10,
		backgroundColor: Colors.lightGreen
	},
	spendMeter: { flexDirection: 'row', paddingHorizontal: 20, justifyContent: 'space-between' },
	cardOptionStyle: { flexDirection: 'row', justifyContent: 'space-between', padding: 20 },
	container: {
		backgroundColor: Colors.blue
	},
	availableBalanceStyle: {
		marginTop: 30,
		marginLeft: 20,
		color: Colors.white,
		...Fonts.AvenirMedium()
	},
	balanceView: { flexDirection: 'row', marginTop: 12, marginLeft: 20, alignItems: 'center', paddingBottom: height },
	amountText: {
		marginLeft: 10,
		color: Colors.white,
		...Fonts.AvenirBold()
	},
	hideButtonStyle: {
		height: 80,
		margin: 20,
		width: width / 2.75,
		backgroundColor: Colors.white,
		borderTopLeftRadius: 8,
		borderTopRightRadius: 8,
		alignSelf: 'flex-end',
		flexDirection: 'row',
		justifyContent: 'center',
		paddingTop: 10
	},
	hideButtonText: {
		marginLeft: 7,
		color: Colors.green,
		...Fonts.AvenirDemiBold(12)
	},
	loading: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		alignItems: 'center',
		justifyContent: 'center'
	},
	title: {
		...Fonts.AvenirMedium(),
		color: Colors.black
	},
	subtitle: {
		...Fonts.AvenirRegular(13),
		color: 'rgba(34, 34, 34, 0.5)'
	}
});

const mapStateToProps = state => ({
	cardInfo: state.card.cardInfo,
	limit: state.card.limit
});

const mapDispatchToProps = { setCardInfo, setLimit };

export default connect(mapStateToProps, mapDispatchToProps)(DebitCardScreen);

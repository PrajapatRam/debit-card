'use strict';
import React from 'react';
import {
	StyleSheet,
	View,
	Text,
	TouchableOpacity,
	SafeAreaView,
	StatusBar,
	Dimensions,
	Image,
	ActivityIndicator,
	TextInput
} from 'react-native';
import { Header, DollarSign } from './Common';
import { Colors, Fonts, Strings, Images } from '../res';
import { APIManager } from '../Networking';
import { connect } from 'react-redux';
import { setLimit } from '../Redux/cardSlice';
const { width } = Dimensions.get('window');

class SetLimitScreen extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			inputValue: ''
		};
	}

	setLimit = limit => {
		this.setState({ loading: true });
		APIManager.updateLimit('1', limit)
			.then(res => {
				this.props.setLimit(res.cards.limit);
				this.setState({ loading: false });
				this.props.navigation.goBack();
			})
			.catch(err => console.log(err));
	};

	renderInputBox = () => (
		<View style={styles.inputContainer}>
			<DollarSign />
			<TextInput
				style={styles.input}
				value={this.state.inputValue}
				onChangeText={text => {
					this.setState({ inputValue: text });
				}}
			></TextInput>
		</View>
	);

	amountButton = amount => (
		<TouchableOpacity style={styles.amountButton} onPress={() => this.setState({ inputValue: amount })}>
			<Text style={styles.amountButtonText}>{`S$ ${amount}`}</Text>
		</TouchableOpacity>
	);

	renderAmountButtons = () => (
		<View style={styles.buttonContainer}>
			{this.amountButton('5,000')}
			{this.amountButton('10,000')}
			{this.amountButton('20,000')}
		</View>
	);

	renderSaveButton = () => (
		<SafeAreaView>
			<TouchableOpacity
				disabled={!this.state.inputValue}
				style={[
					styles.saveButton,
					{
						backgroundColor: this.state.inputValue ? Colors.green : Colors.grey,
						elevation: this.state.inputValue ? 5 : 0
					}
				]}
				onPress={() => {
					this.setLimit(this.state.inputValue);
				}}
			>
				<Text style={{ color: Colors.white, ...Fonts.AvenirDemiBold(16) }}>{'Save'}</Text>
			</TouchableOpacity>
		</SafeAreaView>
	);

	renderContent = () => (
		<View style={styles.contentStyle}>
			<View>
				<View style={{ flexDirection: 'row', marginTop: 20 }}>
					<Image source={Images.SetLimit}></Image>
					<Text style={{ ...Fonts.AvenirMedium(), paddingLeft: 15 }}>{Strings.setLimitDesc}</Text>
				</View>
				{this.renderInputBox()}
				<Text style={{ color: Colors.lightBlack, ...Fonts.AvenirRegular(13), marginTop: 13 }}>
					{Strings.weeklyDesc}
				</Text>
				{this.renderAmountButtons()}
			</View>
			{this.renderSaveButton()}
		</View>
	);

	renderActivityIndicator = () =>
		this.state.loading && (
			<View style={styles.loading}>
				<ActivityIndicator size="large" />
			</View>
		);

	render = () => (
		<View style={styles.container}>
			<SafeAreaView>
				<StatusBar barStyle="light-content" />
				<Header title={Strings.spendingLimit} showBack navigation={this.props.navigation} />
			</SafeAreaView>
			{this.renderContent()}
			{this.renderActivityIndicator()}
		</View>
	);
}

const styles = StyleSheet.create({
	contentStyle: {
		flex: 1,
		marginTop: 50,
		backgroundColor: Colors.white,
		borderTopLeftRadius: 24,
		borderTopRightRadius: 24,
		padding: 25,
		justifyContent: 'space-between'
	},
	saveButton: {
		borderRadius: 30,
		width: width / 1.4,
		paddingVertical: 20,
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'center',
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84
	},
	buttonContainer: { flexDirection: 'row', justifyContent: 'space-between', marginTop: 25 },
	amountButtonText: { color: Colors.green, ...Fonts.AvenirDemiBold(12) },
	amountButton: {
		backgroundColor: Colors.lightGreen,
		borderRadius: 4,
		width: width / 3 - 30,
		paddingVertical: 10,
		alignItems: 'center',
		justifyContent: 'center'
	},
	inputContainer: {
		flexDirection: 'row',
		marginTop: 18,
		alignItems: 'center',
		borderBottomColor: Colors.grey,
		borderBottomWidth: 1,
		paddingBottom: 7
	},
	input: { marginLeft: 10, ...Fonts.AvenirBold() },
	container: {
		backgroundColor: Colors.blue,
		flex: 1
	},
	loading: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		alignItems: 'center',
		justifyContent: 'center'
	}
});

const mapStateToProps = state => ({});

const mapDispatchToProps = { setLimit };

export default connect(mapStateToProps, mapDispatchToProps)(SetLimitScreen);

import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	cardInfo: {},
	limit: null
};

export const cardSlice = createSlice({
	name: 'card',
	initialState,
	reducers: {
		setCardInfo: (state, action) => {
			state.cardInfo = action.payload;
		},
		setLimit: (state, action) => {
			state.limit = action.payload;
		}
	}
});

export const { setCardInfo, setLimit } = cardSlice.actions;

export default cardSlice.reducer;

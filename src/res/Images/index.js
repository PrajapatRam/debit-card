export default {
	AspireLogo: require('./AspireLogo.png'),
	BackIcon: require('./Arrow.png'),
	Logo: require('./Logo.png'),
	VisaLogo: require('./VisaLogo.png'),
	EyeOpen: require('./EyeOpen.png'),
	EyeClosed: require('./EyeClosed.png'),
	Insight: require('./Insight.png'),
	Limit: require('./Limit.png'),
	Freeze: require('./Freeze.png'),
	NewCard: require('./NewCard.png'),
	Deactivate: require('./Deactivate.png'),
	SetLimit: require('./SetLimit.png')
};

import Colors from './Colors';
import Fonts from './Fonts';
import Images from './Images';
import Strings from './Strings';
module.exports = {
	Colors,
	Fonts,
	Images,
	Strings
};

export default {
	white: 'rgba(255, 255, 255, 1)',
	black: 'rgba(34, 34, 34, 1)',
	lightBlack: 'rgba(34, 34, 34, 0.5)',
	green: 'rgba(1, 209, 103, 1)',
	lightGreen: 'rgba(1, 209, 103, 0.07)',
	blue: 'rgba(12, 54, 90, 1)',
	grey: 'rgba(0, 0, 0, 0.12)'
};

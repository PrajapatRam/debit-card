export default {
	AvenirRegular: (fontSize = 14) => ({ fontFamily: 'AvenirNext-Regular', fontSize }),
	AvenirMedium: (fontSize = 14) => ({ fontFamily: 'AvenirNext-Medium', fontSize }),
	AvenirDemiBold: (fontSize = 14) => ({ fontFamily: 'AvenirNext-DemiBold', fontSize }),
	AvenirBold: (fontSize = 24) => ({ fontFamily: 'AvenirNext-Bold', fontSize })
};

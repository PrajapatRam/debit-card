export default class APIManager {
	static getCard = id =>
		new Promise((resolve, reject) => {
			fetch(`/api/cards/${id}`)
				.then(res => res.json())
				.then(json => resolve(json))
				.catch(error => reject(error));
		});

	static updateLimit = (id, limit) =>
		new Promise((resolve, reject) => {
			fetch(`/api/cards/${id}`, {
				method: 'PATCH',
				body: JSON.stringify({
					limit: limit
				})
			})
				.then(res => res.json())
				.then(json => resolve(json))
				.catch(error => reject(error));
		});
}

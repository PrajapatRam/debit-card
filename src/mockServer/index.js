import { createServer, Model } from 'miragejs';

createServer({
	environment: 'development',
	models: {
		cards: Model
	},
	seeds(server) {
		server.create('card', {
			cardinfo: {
				used: '345',
				balance: '3,000',
				cardholdername: 'Mark Henry',
				cardnumber: '5647341124132020',
				validthru: '12/20',
				cvv: '456'
			},
			limit: 100
		});
	},
	routes() {
		this.namespace = 'api/cards';
		this.get('/:id', (schema, request) => {
			let id = request.params.id;
			return schema.cards.find(id);
		});
		this.patch('/:id', (schema, request) => {
			let newAttrs = JSON.parse(request.requestBody);
			let id = request.params.id;
			let card = schema.cards.find(id);
			return card.update(newAttrs);
		});
	}
});

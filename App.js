/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { DebitCardScreen, SetLimitScreen } from './src/UI';
import { store } from './src/Redux/store';
import { Provider } from 'react-redux';

const Stack = createNativeStackNavigator();

const App = () => {
	return (
		<Provider store={store}>
			<NavigationContainer>
				<Stack.Navigator
					initialRouteName="DebitCardScreen"
					screenOptions={{
						headerShown: false
					}}
				>
					<Stack.Screen name="DebitCardScreen" component={DebitCardScreen} />
					<Stack.Screen name="SetLimitScreen" component={SetLimitScreen} />
				</Stack.Navigator>
			</NavigationContainer>
		</Provider>
	);
};

export default App;
